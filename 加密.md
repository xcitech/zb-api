# zb-api 加密方式

- accesskey=08b38d11-445f-447f-9372-d255e10b91bd
- secretKey=ad030fda-86c1-4d9e-b333-fefbc1fb89e8

在线加密工具: http://tool.oschina.net/encrypt?type=2

# 第一步: 使用sha1 加密 secretKey 得到 5b1f87b2375ad8e46abf5e8ac4577c50dcee7989 
![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/170704_c037223f_3577.png "SecretKey sha加密.png")

# 第二步: 
    1. demo获取用户信息,将相应的参数进行拼接得到最终结果:accesskey=08b38d11-445f-447f-9372-d255e10b91bd&method=getAccountInfo
    2. 使用secretKey通过sha1加密的字符串:5b1f87b2375ad8e46abf5e8ac4577c50dcee7989 作为哈希值
    3. 计算得到最终sign: 48a1af2ea626dde38134ff8a51d8d0c3
![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/171301_16bbace9_3577.png "sign.png")

