package zb.ws.api;

import java.io.File;
import java.io.IOException;

import cn.hutool.core.thread.ThreadUtil;
import entity.commons.UserApiEntity;
import entity.enumtype.ExchangeEnum;
import jodd.props.Props;
import lombok.val;
import zb.ws.adapter.AdapterZb;
import zb.ws.adapter.ZbAdapter;

public class TestWsApi {

	public static void main(String[] args) throws IOException {
		Props p = new Props();
		p.load(new File("c:/config/zb.txt"));
		String apiKey = p.getValue("user.zb.apikey");// 修改为自己的公钥
		String secretKey = p.getValue("user.zb.secretKey");// 修改为自己的私钥
//		apiKey = "ce2a18e0-dshs-4c44-4515-9aca67dd706e";
//		secretKey = "c11a122s-dshs-shsa-4515-954a67dd706e";
		
		val userApi = new UserApiEntity(ExchangeEnum.zb, "测试", apiKey,secretKey);
//		val apiConfig = new ApiConfig("wss://api.zb.com:9999/websocket", "192.168.5.182", 23128);
		val api = new WsZb(userApi, new AdapterZb());
//		val api = new WsZb(userApi, new ZbAdapter());
		ThreadUtil.sleep(2000);
		
		String symbol= "usdtqc";
//		api.saveChannel("zbqc_depth");
//		api.saveChannel("ltcbtc_trades");
		
//		api.getAccount();
		
//		api.buy(symbol, 1, 0.001);
//		api.getOrder(symbol, 20180522105585216l, "测试");
//		api.cancel(symbol, 20180522105585216l, "取消");
//		
//		api.getOrders(1, 1, symbol);
//		api.getOrdersIgnoreTradeType(1, 10, symbol);
//		api.getUnfinishedOrdersIgnoreTradeType(1, 10, symbol);
//		api.getOrdersNew(1, 10, 1, symbol);
		api.getLeverAssetsInfo();
//		api.getLeverBills("qc");//内部错误
////		api.transferInLever();
//		api.transferOutLever();//内部错误
//		api.loan();//内部错误
//		api.cancelLoan();
//		api.getLoans();
//		api.getLoanRecords();
//		api.borrow();
//		api.repay();
//		api.getRepayments();
	}
}

