## 获取K线
```request
GET http://api.zb.cn/data/v1/kline?market=btc_usdt
```
```response
{
    "data": [
        [
            1472107500000,
            3840.46,
            3843.56,
            3839.58,
            3843.3,
            492.456
        ]...
    ],
    "moneyType": "usdt",
    "symbol": "btc"
}
```
**请求参数**

参数名称 | 类型 | 取值范围
---|---|---
market | String | 例：btc_qc
type | String | 1min/3min/5min/15min/30min/1day/3day/1week/1hour/2hour/4hour/6hour/12hour
since | long | 从这个时间戳之后的
size | int | 返回数据的条数限制(默认为1000，如果返回数据多于1000条，那么只返回1000条)

```resdata
data : K线内容
moneyType : 买入货币
symbol : 卖出货币
data : 内容说明
[
1417536000000, 时间戳
2370.16, 开
2380, 高
2352, 低
2367.37, 收
17259.83 交易量
]
```

```java
public void getKline() {
	//得到返回结果
	String returnJson = HttpRequest.get("http://api.zb.cn/data/v1/kline?market=btc_usdt").send().bodyText();
}
```

```python
def get(self, url):
    while True:
        try:
            r = requests.get(url)
        except Exception:
            time.sleep(0.5)
            continue
        if r.status_code != 200:
            time.sleep(0.5)
            continue
        r_info = r.json()
        r.close()
        return r_info
        
def getKline(self, market):
    url = 'http://api.zb.cn/data/v1/kline?market=btc_usdt'
    return self.get(url)
```