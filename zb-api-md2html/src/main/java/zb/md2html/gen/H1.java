package zb.md2html.gen;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import jodd.io.FileUtil;
import jodd.io.findfile.FindFile;
import jodd.template.MapTemplateParser;
import kits.my.BufferKit;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.extern.slf4j.Slf4j;
import zb.md2html.api.IHtml;
import zb.md2html.kit.MdKit;

/**
 * 将所有h2内容包含h1组装
 * 
 * @author Administrator
 *
 */
@Slf4j
@RequiredArgsConstructor
public class H1 extends ZbBase implements IHtml {
	private @NonNull String projectPath;

	/**
	 * 需要生成All.js的文件,对应所有的h2id,否则代码块在html中显示不出来
	 */
	private void genAllJs(Set<String> h2Ids) {
		val str = "\"#" + CollUtil.join(h2Ids, "\",\"#") + "\"";

		val params = new HashMap<String, String>();
		params.put("allJsParams", str);
		try {
			val html = new MapTemplateParser().of(params).parse(FileUtil.readString(allJs));
			val file = new File(projectPath + "public\\src\\api\\javascripts\\all.js");
			FileUtil.writeString(file, html);
			log.info("生成all.js文件,路径:{}", file.getAbsolutePath());
		} catch (IOException e) {
			log.info("生成all.js异常", e);
		}
	}

	@Override
	public String getApiContent() {
		val sbRoot = new BufferKit();
		val h2Ids = new HashSet<String>();
		// 第一层目录指定到 api/v1 版本目录
		FindFile.create().searchPath(fileApi).findAll().forEach(root -> {
			// 缓存rest_api_info.md | ws info 内容
			val sb = new BufferKit();
			// 第二层指定到接口类型目录
			FindFile.create().searchPath(root).findAll().forEach(apiFolder -> {
				if (!apiFolder.isDirectory()) {
					// 如果是md文件,读取并放在第一
					try {
						sbRoot.append(MdKit.readMd2Str(apiFolder));
					} catch (IOException e) {
						log.error("api描述文件异常",e);
					}
					return;
				}
				String h1Name = ReUtil.replaceAll(apiFolder.getName(), "[1-9]", "").trim();
				//新需求要去掉目录的api名称
				h1Name = h1Name.replace("restApi", "").replace("websocketApi", "");
				log.debug("h1-目录:" + h1Name);
				val h1 = StrUtil.format(h1Temp, h1Name, h1Name);
				sb.append(h1);

				val sbh2 = new BufferKit();
				// 第三层指定到接口文件目录
				FindFile.create().searchPath(root + "/" + apiFolder.getName()).findAll().forEach(fapi -> {
					log.debug("h2--" + fapi);
					try {
						val mdLines = Arrays.asList(FileUtil.readLines(fapi));
						val h2Info = new H2(mdLines).getH2();
						sbh2.append(h2Info.getHtml());
						h2Ids.add(h2Info.getId());
					} catch (IOException e) {
						log.error(fapi.getName(), e);
					}
				});
				sb.append(sbh2.toString());
			});
			sbRoot.append(sb.toString());
		});
		this.genAllJs(h2Ids);
		return sbRoot.toString();
	}
}
